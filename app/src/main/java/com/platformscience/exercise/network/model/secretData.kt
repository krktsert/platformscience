package com.platformscience.exercise.network.model

data class secretData(
    val drivers: List<String>,
    val shipments: List<String>
)