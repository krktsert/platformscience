package com.platformscience.exercise.network.service

import com.platformscience.exercise.network.model.secretData
import retrofit2.Response
import retrofit2.http.GET

interface GithubService {

    @GET("v1/feed")
    suspend fun getSecretData(): Response<secretData>

}