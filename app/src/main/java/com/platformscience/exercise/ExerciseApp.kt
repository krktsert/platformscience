package com.platformscience.exercise

import android.app.Application
import com.platformscience.exercise.dagger.AppComponent
import com.platformscience.exercise.dagger.AppModule
import com.platformscience.exercise.dagger.DaggerAppComponent

/**
 * The application class - an entry point into our app where we initialize Dagger.
 */

class ExerciseApp : Application() {
    companion object {
        private lateinit var appComponent: AppComponent

        fun getAppComponent(): AppComponent {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerAppComponent()
    }

    private fun initDaggerAppComponent(): AppComponent {
        appComponent =
            DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        return appComponent
    }
}