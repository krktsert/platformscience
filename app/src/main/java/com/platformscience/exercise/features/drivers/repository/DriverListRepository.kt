package com.platformscience.exercise.features.drivers.repository

import com.platformscience.exercise.features.drivers.model.Destination
import com.platformscience.exercise.features.drivers.model.Driver
import com.platformscience.exercise.network.service.GithubService
import javax.inject.Inject

class DriverListRepository @Inject constructor(val githubService: GithubService) {

    //Check if local data contains drivers or not
    //return the data based on that

    //Just to be able to get the code running
    var availableDriver: List<Driver> = arrayListOf("Everardo Welch",
        "Orval Mayert",
        "Howard Emmerich",
        "Tzaiah Lowe",
        "Monica Hermann",
        "Ellis Wisozk",
        "Noemie Murphy",
        "Cleve Durgan",
        "Murphy Mosciski",
        "Kaiser Sose").toList().map {
            Driver(it)
    }

    //Just to be able to get the code running
    var targetDestinations: List<Destination> = arrayListOf("215 Osinski Manors",
        "9856 Marvin Stravenue",
        "7127 Kathlyn Ferry",
        "987 Champlin Lake",
        "63187 Volkman Garden Suite 447",
        "75855 Dessie Lights",
        "1797 Adolf Island Apt. 744",
        "2431 Lindgren Corners",
        "8725 Aufderhar River Suite 859",
        "79035 Shanna Light Apt. 322").toList().map {
        Destination(it)
    }

    suspend fun getDrivers(): List<Driver>{
        if (availableDriver.isEmpty()) {
            val call = githubService.getSecretData()
            if(call.isSuccessful) {
                availableDriver =  call.body()!!.drivers.map {
                    Driver(it)
                }
                return availableDriver
            }
        }
        return availableDriver
    }

    suspend fun getDestinations(): List<Destination> {
        if (targetDestinations.isEmpty()) {
            val call = githubService.getSecretData()
            if(call.isSuccessful) {
                targetDestinations =  call.body()!!.shipments.map {
                    Destination(it)
                }
                return targetDestinations
            }
        }
        return targetDestinations
    }


}