package com.platformscience.exercise.features.drivers.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.platformscience.exercise.features.drivers.model.Driver
import com.platformscience.exercise.features.drivers.repository.DriverListRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class DriversViewModel @Inject constructor(val repository: DriverListRepository): ViewModel() {

    private val _driverList: MutableLiveData<List<Driver>> by lazy {
        MutableLiveData<List<Driver>>().apply {
            emptyList<Driver>()
        }
    }

    val driverList: LiveData<List<Driver>> = _driverList

    fun getDrivers() {
        viewModelScope.launch {
            _driverList.value = repository.getDrivers()
        }.invokeOnCompletion {
            //obviously if there is an error it needs to be handled
            executeSecretAlgorithm()
        }
    }

    private fun executeSecretAlgorithm() {
        //create table, rows for drivers, columns for destinations, score is kept in the cell
        val table =
            Array(repository.availableDriver.size) { Array(repository.targetDestinations.size) { 0.0 } }

        for (i in table.indices) {
            for (j in table[0].indices) {
                if (repository.targetDestinations[j].address.length % 2 == 0) {
                    table[i][j] = repository.availableDriver[i].getVowelCount() * 1.5
                } else {
                    table[i][j] = repository.availableDriver[i].getConsonantCount() * 1.0
                }

                if (isMultipleCommonFactorShared(
                        repository.availableDriver[i].driverName.length,
                        repository.targetDestinations[j].address.length )) {
                    table[i][j] *= 1.5
                }
            }
        }

        //find best scored driver for that destination, keep the index
        for (j in table[0].indices) {
            var maxIndex = 0
            var max = -1.0
            for (i in table.indices) {
                if(table[i][j] > max) {
                    maxIndex = i
                    max = table[i][j]
                }
            }
            Log.d("KRKT", "Setting Driver:${repository.availableDriver[maxIndex]} to  ${repository.targetDestinations[j]}")
            if( repository.availableDriver[maxIndex].destination == null) {
                repository.availableDriver[maxIndex].destination = repository.targetDestinations[j]
                //clear rest of the scores for that address.
                table[maxIndex] = Array(table[j].size) {-1.0}
            }
        }

    }

    private fun isMultipleCommonFactorShared(driver: Int, address: Int): Boolean {
        val smaller =  if(driver < address) { driver } else { address }
        var commonFactorCount = 0
        for (i in 2..smaller) {
            if ( i % driver == 0 && i % address == 0) {
                commonFactorCount++
            }
            if (commonFactorCount >= 1) {
                return true
            }
        }

        return false
    }


}