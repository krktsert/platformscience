package com.platformscience.exercise.features.drivers

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.platformscience.exercise.ExerciseApp
import com.platformscience.exercise.R
import com.platformscience.exercise.ViewModelFactory
import com.platformscience.exercise.features.drivers.viewmodel.DriversViewModel
import javax.inject.Inject

/**
 * A fragment representing a list of Items.
 */
class DriversFragment : Fragment() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var driverListAdapter: DriverListAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<DriversViewModel>
    val viewModel: DriversViewModel by lazy {
        viewModelFactory.get<DriversViewModel>(
            requireActivity()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        ExerciseApp.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_drivers_list, container, false)
        recyclerView = view.findViewById(R.id.list)

        viewModel.getDrivers()

        driverListAdapter = DriverListAdapter()

        viewModel.driverList.observe (viewLifecycleOwner, {
            driverListAdapter.updateData(it)
        })

        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = driverListAdapter
        }

        return view
    }
}