package com.platformscience.exercise.features.drivers

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.platformscience.exercise.R
import com.platformscience.exercise.databinding.FragmentDriversBinding
import com.platformscience.exercise.features.driverdetails.DriverDetailsFragment
import com.platformscience.exercise.features.drivers.model.Driver

class DriverListAdapter() : RecyclerView.Adapter<DriverListAdapter.ViewHolder>() {
    private var values: List<Driver> = emptyList()

    fun updateData(drivers: List<Driver>) {
        values = drivers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentDriversBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.nameView.text = item.driverName



        holder.nameView.setOnClickListener {
            val detailsFragment = DriverDetailsFragment()
            val b = Bundle()
            b.putString(DriverDetailsFragment.DRIVER_NAME, item.driverName)
            b.putString(DriverDetailsFragment.DESTINATION, item.destination!!.address)
            detailsFragment.arguments = b

            (it.context as AppCompatActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.container, detailsFragment, "")
                .addToBackStack("")
                .commit()
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentDriversBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val nameView: TextView = binding.name
    }

}