package com.platformscience.exercise.features.drivers.model

data class Destination(val address: String)
