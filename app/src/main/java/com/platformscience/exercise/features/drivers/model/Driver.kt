package com.platformscience.exercise.features.drivers.model

data class Driver(val driverName: String){
    private var vowels: Int = -1
    private var consonants: Int = -1
    var destination: Destination? = null

    fun getVowelCount() : Int {
        if(vowels != -1) {
            return vowels
        } else {
            analyzeName()
            return vowels
        }
    }

    fun getConsonantCount() : Int {
        if(consonants != -1) {
            return consonants
        } else {
            analyzeName()
            return consonants
        }
    }


    private fun analyzeName() {
        val line = driverName.lowercase()
        for (ch in line) {
            if (ch == 'a' || ch == 'e' || ch == 'i'
                || ch == 'o' || ch == 'u') {
                vowels++
            } else if (ch in 'a'..'z') {
                ++consonants
            }
        }
    }
}
