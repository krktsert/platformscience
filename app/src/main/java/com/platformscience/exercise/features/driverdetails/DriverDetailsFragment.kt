package com.platformscience.exercise.features.driverdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.platformscience.exercise.R

class DriverDetailsFragment : Fragment() {

    private var driverName: String? = null
    private var destination: String? = null
    companion object {
        val DRIVER_NAME = "DRIVER_NAME"
        val DESTINATION = "DESTINATION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            driverName = it.getString(DRIVER_NAME)
            destination = it.getString(DESTINATION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v =  inflater.inflate(R.layout.fragment_driver_details, container, false)

        v.findViewById<TextView>(R.id.driver_name).text = driverName
        v.findViewById<TextView>(R.id.destination_address).text = destination

        return v
    }

}