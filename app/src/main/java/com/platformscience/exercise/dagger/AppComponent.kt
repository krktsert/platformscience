package com.platformscience.exercise.dagger

import com.platformscience.exercise.ExerciseApp
import com.platformscience.exercise.MainActivity
import com.platformscience.exercise.features.drivers.DriversFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        fun appModule(module: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: ExerciseApp)
    fun inject(activty: MainActivity)
    fun inject(driversFragment: DriversFragment)
}
