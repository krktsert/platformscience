package com.platformscience.exercise.dagger

import android.app.Application
import com.platformscience.exercise.ExerciseApp

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule constructor(private val application: ExerciseApp) {
    @Provides
    @Singleton
    fun getApplication(): Application {
        return application
    }
}
