package com.platformscience.exercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.platformscience.exercise.features.drivers.DriversFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("KRKT", "SERT")

        val driversFragment = DriversFragment()

        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container, driversFragment,
                "driversFragment.TAG"
            )
            .commit()


    }
}